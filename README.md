# Project Title

An optimisation challenge

## Getting Started

I have set up an input facility for additional testing.

I have published a 'lite' version on Databricks community edition for easy verification which exludes unit tests and a few small pieces of functionality:
* https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/3828607418809250/1479354958181500/5023710063065610/latest.html

**The Databricks copy excludes unit tests**

* README.md	readme        - readme
* positive_test_output.py - expected test results
* rubix_challenge.py      - optimiser
* unit_test.py            - unit tests

Apart from the unit tests.

### Prerequisites

* Python
* Pulp

## Running the tests

To execute unit tests, execute the unit_test.py file

### The tests

There are 2 tests, a dummy test and a test specifically for the values provided.

## Authors

* **Jaco Silvis**
