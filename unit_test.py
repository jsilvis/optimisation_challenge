import unittest
import warnings
from rubix_challenge import optimise_list
from positive_test_output import positive_test_results, positive_test_inputs

#set constants
items = {
                'VS5':{'5':8.99, '3': 6.99},
                'MB11':{'8':24.95,'5':16.95,'2':9.95},
                'CF':{'9':16.99,'5':9.95,'3':5.95}
                }


class Tests(unittest.TestCase):

    def test(self):
        # dummy test
        self.assertEqual( 1, 1)

    def test_optimiser(self):
        # test optimiser vs test results in positive_test_output.py file

        # disabel pulp deprecation and resourcewarnings warnings
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore",category=DeprecationWarning)
            warnings.simplefilter("ignore", category=ResourceWarning)

            # run a test for each item in dict
            for test in positive_test_inputs:
                print('Running test: {}'.format(test))
                test_input = positive_test_inputs[test]
                test_output = positive_test_results[test]
                #set up
                test_result = (optimise_list(items, test_input))
                #test
                self.assertEqual(test_result,test_output)


if __name__ == '__main__':
    unittest.main()
