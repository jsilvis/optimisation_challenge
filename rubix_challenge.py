import pulp

def optimise(item,total_amout):
  model = pulp.LpProblem("Optmise {}".format(item), pulp.LpMinimize)

  # generator - create variables for each item size in input dict
  index = (i for i in item)

  # declare a variable for each pack_size
  item_cost = pulp.LpVariable.dicts("pack_size",
                                         index,
                                         lowBound=0,
                                         cat='Integer')

  # create model for item in input
  # (8.99 * x) + (6.99 * y) = cost
  model += (
    pulp.lpSum([
      item[i]*item_cost[i]
      for i in item
      ])
    )

  # add constraints
  # 5 * x + 3 * y = total_amout
  model += pulp.lpSum([
    int(i)*item_cost[i]
    for i in item
    ]) == total_amout

  # solve
  model.solve()
  pulp.LpStatus[model.status]

  # print result
  result = ''
  has_value = False
  for var in item_cost:
    var_value = item_cost[var].varValue
    if int(var_value) > 0:
      has_value = True
      result +=("{} x {} @ ${:.2f}\n".format(int(var_value), var[0], item[var]))

  if int(pulp.value(model.objective)) > 0 and has_value:
      result += ("Total cost: ${:.2f}\n".format(pulp.value(model.objective)))
  else:
      result += "configuration not available"

  return(result)

def get_input(items):
    ordered_items = {}
    for item in items:
        ordered_items[item] = int(input("How many {} would you like: ".format(item)))

    return(ordered_items)

def optimise_list(items, ordered_items):
    result = ''
    for item in items:
        if ordered_items[item] > 0:
            result+= ('{} x {}:\n'.format(ordered_items[item],item))
            result+= optimise(items[item],ordered_items[item])

    return(result)

if __name__ == '__main__':
    # const - items and prices - 'item':{'pack_size':cost}
    items = {
            'VS5':{'5':8.99, '3': 6.99},
            'MB11':{'8':24.95,'5':16.95,'2':9.95},
            'CF':{'9':16.99,'5':9.95,'3':5.95}
            }

    #uncomment for a test run
    #ordered_items = {'VS5':10,'MB11':14,'CF':13}

    ordered_items = get_input(items)

    print(optimise_list(items, ordered_items))
