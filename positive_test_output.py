# for each test, add a test result and a test input

positive_test_results = {'optimise_list_test_1':'''10 x VS5:
2 x 5 @ $8.99
Total cost: $17.98
14 x MB11:
2 x 5 @ $16.95
2 x 2 @ $9.95
Total cost: $53.80
13 x CF:
2 x 5 @ $9.95
1 x 3 @ $5.95
Total cost: $25.85
''',
'optimise_list_test_2':'''33 x VS5:
6 x 5 @ $8.99
1 x 3 @ $6.99
Total cost: $60.93
11 x MB11:
1 x 5 @ $16.95
3 x 2 @ $9.95
Total cost: $46.80
7 x CF:
configuration not available'''}

positive_test_inputs = {'optimise_list_test_1':{'VS5':10,'MB11':14,'CF':13},
                        'optimise_list_test_2':{'VS5':33,'MB11':11,'CF':7}}
